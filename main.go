package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	e "bitbucket.org/gotamer/errors"
	"bitbucket.org/gotamer/final"
)

const (
	APPNAME = "earthporn"
	PS      = string(os.PathSeparator)
)

var (
	fileLogger  *os.File
	help        = flag.Bool("h", false, "Display this help")
	name        = flag.String("n", "earthporn", "Subreddit Name")
	folder      = flag.String("f", os.Getenv("HOME")+PS+".wallpapers", "Local Wallpaper folder (must exist)")
	sleep       = flag.Int("s", 10, "Sleep seconds between images")
	screensaver = flag.Bool("r", false, "Run the Screen Saver")
)

type Link struct {
	Id        string `json:"id"`
	Title     string
	Url       string
	Thumbnail string
	Score     int
	Is_self   bool
}

type Children struct {
	Kind string
	Data *json.RawMessage
}

var (
	links   []Link
	archive []Link
)

func init() {
	var err error
	var filename = os.TempDir() + PS + "earthporn.log"
	fileLogger, err = os.Create(filename)
	e.Fail(err)
	e.ENVIRONMENT = e.ENV_PROD
	e.Logger(fileLogger)
	//err = e.New("Starting")
	e.Check(err)
}

func end() {
	fileLogger.Close()
}

func main() {
	var err error
	flag.Parse()
	if *help == true {
		flag.PrintDefaults()
		os.Exit(0)
	}
	path := *folder + PS + "." + *name + ".json"
	load(path, &archive)

	getJson("http://www.reddit.com/r/" + *name + ".json")
	for _, v := range links {
		switch {
		case v.Is_self == true:
			continue
		case matchJpeg(v.Url):
			continue
		case matchArchive(v.Id) == true:
			continue
		}

		var data []byte
		response, err := http.Get(v.Url)
		e.Fail(err)
		defer response.Body.Close()
		data, err = ioutil.ReadAll(response.Body)
		e.Fail(err)
		err = ioutil.WriteFile(*folder+PS+v.Id+".jpeg", data, 0644)
		e.Fail(err)
		response.Body.Close()
		archive = append(archive, v)
	}
	err = save(path, archive)
	e.Fail(err)
	if *screensaver {
		run()
	} else {
		end()
	}
}

func run() {
	feh, err := exec.LookPath("feh")
	if err != nil {
		fmt.Println("Pleaes install feh (apt-get install feh)")
		e.Fail(err)
	}

	final.AppName(APPNAME)
	final.Add(end)
	var path string
	var count int = 0
	var length = len(archive)
	var s = time.Duration(*sleep)
	for {
		path = *folder + PS + archive[count].Id + ".jpeg"
		cmd := exec.Command(feh, "--bg-max", path)
		err := cmd.Start()
		e.Fail(err)
		err = cmd.Wait()
		e.Fail(err)

		time.Sleep(s * time.Second)
		count = count + 1
		if count >= length {
			count = 0
		}
	}
}

func matchJpeg(s string) bool {
	if strings.HasSuffix(s, ".jpg") {
		return false
	}
	if strings.HasSuffix(s, ".jpeg") {
		return false
	}
	return true
}

func matchArchive(id string) bool {
	for _, a := range archive {
		if id == a.Id {
			return true
		}
	}
	return false
}

func getJson(url string) {

	var children []Children
	var objmap map[string]*json.RawMessage
	var datamap map[string]*json.RawMessage
	var link *Link

	response, err := http.Get(url)
	e.Fail(err)
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	e.Fail(err)

	err = json.Unmarshal(data, &objmap)
	e.Fail(err)

	err = json.Unmarshal(*objmap["data"], &datamap)
	e.Fail(err)
	err = json.Unmarshal(*datamap["children"], &children)
	e.Fail(err)
	for _, v := range children {
		err = json.Unmarshal(*v.Data, &link)
		e.Fail(err)
		links = append(links, *link)
	}
}

// load gets your config from the json file,
// and fills your struct with the option
func load(filename string, o interface{}) error {
	b, err := ioutil.ReadFile(filename)
	if err == nil {
		err = json.Unmarshal(b, &o)
	}
	return err
}

// Save will save your struct to the given filename,
// this is a good way to create a json template
func save(filename string, o interface{}) error {
	j, err := json.MarshalIndent(&o, "", "\t")
	if err == nil {
		err = ioutil.WriteFile(filename, j, 0660)
	}
	return err
}
