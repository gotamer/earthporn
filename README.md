Reddit Earthporn wallpaper downloader and slideshow
====================================================

Photos of our planet by the people of http://reddit.com/r/EarthPorn

Downloades beautiful photos of our planet. Each photo is sourced from the users of http://reddit.com/r/earthporn and credit goes to each user for every photo.


	earthporn -h

	-h=false: Display this help
	-f="/home/you/.wallpapers": Local Wallpaper folder (must exist)
	-n="earthporn": Subreddit Name
	-r=false: Run the Screen Saver
	-s=10: Sleep seconds between images


Executable:
-----------

For [Linux AMD64](https://bitbucket.org/gotamer/earthporn/downloads)


Dependency
----------
*feh* image viewer

	apt-get install feh


Install from source with go:
----------------------------

	go get bitbucket.org/gotamer/earthporn
	cd $GOPATH/src/bitbucket.org/gotamer/earthporn
	go install

TODO
----

 - Clean up code
 - Delete old images (max image count)
 - Reindex database in case user deletes images in file browser

